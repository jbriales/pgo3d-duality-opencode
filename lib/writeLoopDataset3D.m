function [nrNodes] = writeLoopDataset3D(nrNodes, r, sigmaT, sigmaR, filename) 
% Creates a 3D pose graph optimization with ground truth nodes equally 
% spaced along a circle. 
% - nrNodes: number of nodes in the problem (the graph has exactly nrNodes edges as well)
% - r: radius of the circle
% - sigmaT and sigmaR: are the std of the translation and rotation noise
% - filename: name of the file where the pose graph is written (in g2o format)
%
% Author: Luca Carlone
% Georgia Institute of Technology
% Date: May 2014 

% addpath(genpath('../')); % include functionality to write g2o file
doPlot = 1;
fixElevation = 1; % otherwise the circle becomes a spiral
I3 = eye(3);

%% Default parameters
if nargin < 1
  nrNodes = 20; % number of poses
  r = 50; % radius of the circle
  sigmaT = 0.05;
  sigmaR = 0.05;  
  filename = horzcat('/home/aspn/Desktop/TRO/MOLE2Dmatlab/cleanDatasets/3D/loop3D.g2o');
end

if(doPlot) figure; hold on; end

%% Create ground truth
xyz=[]; % this is created as a matrix for plot purposes
for alfa_deg = linspace(0, 360, nrNodes) % 0:20:360 % angle in degrees   
   alfa = alfa_deg*pi/180; % angle in radiants
   xl = r * sin(alfa);
   yl = -(r * cos(alfa) - r);
   zl = 2*alfa;
   if (fixElevation) zl = 0; end
   xyz = [xyz;  xl yl zl];
end

%% create positions and orientations
for i=1:nrNodes
    poses(i).t = xyz(i,:)';
    if i==1
        poses(1).R=eye(3);
    else
        thVect = 2 * pi * (rand(3,1)-0.5);
        poses(i).R = rot_exp(I3,rot_hat(I3,thVect));
        % p_next = pose(i).p;
        % p_prev = pose(i-1).p;
        % bearing(p_next,p_prev);
        if(doPlot) plot3( poses(i).t(1), poses(i).t(2), poses(i).t(3),'o'); end
    end    
end

%% create odometric edges
m = nrNodes-1;
for k=1:m
  id1 = k;
  id2 = k+1;
  edges_id(k,1:2) = [id1 id2];
  [tij,Rij] = poseSubNoisy3D(poses(id1), poses(id2), sigmaT, sigmaR);
  measurements.between(k).R = Rij;
  measurements.between(k).t = tij;
  measurements.between(k).Info = [(1/sigmaT^2) * eye(3), zeros(3,3);  
                                      zeros(3,3)       , (1/sigmaR^2) * eye(3) ];
  if(doPlot)
    t1 = poses(id1).t; % gt positions
    t2 = poses(id2).t;
    plot3( [t1(1) t2(1)], [t1(2) t2(2)], [t1(3) t2(3)],'-');
  end
end

%% add loop closure (only 1)
k=m+1;
id1 = k;
id2 = 1;
edges_id(k,1:2) = [id1 id2];
[tij,Rij] = poseSubNoisy3D(poses(id1), poses(id2), sigmaT, sigmaR);
measurements.between(k).R = Rij;
measurements.between(k).t = tij;
measurements.between(k).Info = [(1/sigmaT^2) * eye(3), zeros(3,3);
                               zeros(3,3)       , (1/sigmaR^2) * eye(3) ];

poseOdom = odometryFromEdges3D(measurements,nrNodes);
writeG2oDataset3D(filename, measurements, edges_id, poseOdom);