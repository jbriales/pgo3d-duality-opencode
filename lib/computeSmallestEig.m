function [minEigHlamHat, eigTime] = computeSmallestEig(Hlam, algorithm)

if nargin < 2
  algorithm = 'closestToNegNumber'; % full, sparse, inexact, svds, sparseMax, fixedShift, closestToNegNumber,
end

isDebug = 1;
% save HlamFile

fprintf('Computing eigenvalue using %s algorithm..\n', algorithm)
tim = tic;
switch algorithm
  case 'zero'
    warning('skipping eig computation')
    % Slow version
    minEigHlamHat = 0
  
  case 'full'
    % Slow version
    eigsHlamHat = eig(full(Hlam));
    minEigHlamHat = min(eigsHlamHat)
    
  case 'closestToNegNumber' % closest eig to -1
    opts.issym = 1;
    opts.maxit = 10000;
    negNumber = -100
    minEigHlamHat = eigs(Hlam,1,negNumber,opts)
    
  case 'sparse'
    % Faster version, exploits the fact that eigs(A,1,'SM') returns the smallest **magnitude** eigenvalue.
    % The trick: first shift the spectrum of eigsHlamHat to have positive
    % eigenvalues, call eigs, and reshift the eigenvalues
    % The shift: dhatViaPrimal + minEigHlamHat*(n+1) < fhat
    if(isDebug) disp('Computing largest eigenvalue'); end
    maxEigHlamHat = eigs(Hlam,1,'LM') % largest
    if(isDebug) disp('Computing smallest eigenvalue'); end
    minEigHlamHat = eigs(Hlam + abs(maxEigHlamHat) * speye(size(Hlam)), 1, 'SM') - abs(maxEigHlamHat)
    if(isDebug) disp('Done eigenvalues'); end
    
  case 'sparseMax'
    opts.issym = 1;
    opts.maxit = 10000;
    if(isDebug) disp('Computing largest eigenvalue'); end
    maxEigHlamHat = eigs(Hlam,1,'LM',opts) % largest
    if(isDebug) disp('Computing smallest eigenvalue'); end
    % Hlam + abs(maxEigHlamHat) * speye(size(Hlam) has all positive eigs
    % -Hlam - abs(maxEigHlamHat) * speye(size(Hlam) has all negative eigs
    % largest will be negative
    % opts.disp = 1;
    minEigHlamHat = eigs(Hlam - abs(maxEigHlamHat) * speye(size(Hlam)),1,'LM',opts) + abs(maxEigHlamHat)
    if(isDebug) disp('Done eigenvalues'); end
    
  case 'fixedShift'
    shift = 100;
    if(isDebug) fprintf('Computing smallest eigenvalue (shift=%f)\n', shift); end
    minEigHlamHat = eigs(Hlam + shift,1,0) - shift
    if(isDebug) disp('Done eigenvalues'); end
    
  case 'mysvds'
    if(isDebug) disp('Computing largest singular value'); end
    % [~,maxEigHlamHat] = mysvds(Hlam); % largest
    maxEigHlamHat = eigs(Hlam,1,'LM') % largest
    if(isDebug) disp('Computing smallest singular value'); end
    [~,minEigHlamHat] = mysvds(Hlam + abs(maxEigHlamHat) * speye(size(Hlam))) - abs(maxEigHlamHat);
    minEigHlamHat
    if(isDebug) disp('Done with svds'); end
    
  case 'inexact'
    % Approximate version, computes the eigenvalue closer to -10
    minEigHlamHat = eigs(Hlam, 1, -10);
    
  otherwise
    error('wrong choice of algorithm')
end
eigTime = toc(tim);
fprintf('done with eigenvalue computation (time: %f)!\n',eigTime)