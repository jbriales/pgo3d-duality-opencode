function [tij,Rij] = poseSubNoisy3D(pose_i , pose_j, sigmaT, sigmaR)
% poseSubNoisy3D(p_i , p_j) = inv(p_i) * p_j * p_noise
% returns the 3D roto-translation that tranform p_i to p_j
% plus noise (sigmaT is the std of the noise on the cartesian components,
% sigmaR is the std of the noise added to the angle before wrapping)
%
% Author: Luca Carlone
% Date: May 2014

if nargin < 3
  sigmaT = 0; 
  sigmaR = 0;
end

%% GT measurements
Ri = pose_i.R;
Rj = pose_j.R;
Rij = Ri' * Rj;
ti = pose_i.t;
tj = pose_j.t;
tij = Ri' * (tj - ti);

%% Nominal noise
eRotVec  = sigmaR * randn(3,1); %gaussian_noise(sigmaR^2 * eye(3));
eTranVec = sigmaT * randn(3,1); % gaussian_noise(sigmaT^2 * eye(3));

%% Add noise to measurements 
if(norm(eRotVec)>1e-7)
  eRot = vrrotvec2mat([eRotVec'/norm(eRotVec) norm(eRotVec)]);
  Rij = Rij * eRot; % perturb the rotation measurement
end
tij = tij + eTranVec; 

end