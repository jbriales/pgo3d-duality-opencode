function [dstar, gammaAnc, lambdaMatAnc, optTimeAnc, primalViaDualSol, primalViaDualTime] = SDP_Pose3D_v20(H_pose, measurements, costPose3DChord, cvxQuiet)
% Solves anchored SDP with sum lambda + gamma in the objective
% costPose3D is only used to display comparisons

disp('*********************  SDP_Pose3D_v20  *****************************')
n = measurements.n;
ZZZ = spalloc( 3*n, 3*n, 0); % matrix of zeros
%% Solving dual problem
cvx_quiet(cvxQuiet);
sdpTim = tic;
cvx_precision('best')
cvx_begin SDP
variable lambdaMatAnc(3*n, 3*n) symmetric
variable gammaAnc(1)
maximize( 0.5*(trace(lambdaMatAnc) + gammaAnc) );
subject to
% structure of lambdaMatAnc
for i=1:n
  for j=3*i+1:3*n
    lambdaMatAnc(3*i-2:3*i,j) == zeros(3,1);
  end
  for j=3*i-3:-1:1
    lambdaMatAnc(3*i-2:3*i,j) == zeros(3,1);
  end
end
% PSD
H_pose + blkdiag( ZZZ, -kron(lambdaMatAnc, eye(3)) , -gammaAnc ) >= 0;
cvx_end

optTimeAnc = toc(sdpTim);
dstar = 0.5*(trace(lambdaMatAnc) + gammaAnc);
fprintf('## Optimal value from SDP v2 (Pose3D, anchored) (CPU time: %f):\n %.10E <= cost: %.10E\n', optTimeAnc, dstar, costPose3DChord);
disp('====================================================================')

%% Solving primal via dual
H_pose_Lam = H_pose + blkdiag( ZZZ, -kron(lambdaMatAnc, eye(3)) , -gammaAnc );
[primalViaDualSol, primalViaDualTime] = solvePrimalViaDualPose3D(H_pose_Lam, measurements, costPose3DChord);
