function equalWithTol = checkEqualWithTol(scalar1,scalar2,relTol,arg)

if nargin < 4
    arg = 'printError';
end

equalWithTol = 1;
numZero = 1e-7; % numerical zero
if(scalar1>numZero)
  if abs(scalar2-scalar1)/scalar2 > relTol % these two by construction should give the same primal optimal solution
    equalWithTol = 0;
    if ~strcmp(arg,'noError')
        scalar2, scalar1, error('scalar1 ~= scalar2')
    end
  end
else % costPose3Ddual_v1 is small
  if scalar2 > numZero
    equalWithTol = 0;
    if ~strcmp(arg,'noError')
        scalar2, scalar1, error('scalar1 >> scalar2')
    end
  end
end

