function cost3D = evaluateAngCostRot3D(poses, measurements, edges_id, invert)

if nargin < 4
  invert = 0;
end

m = size(edges_id,1);
nrNodes = length(poses);
if invert
  for i=1:nrNodes
    poses(i).R = poses(i).R';
  end
end

for k=1:m % for each measurement
  id1 = edges_id(k,1);
  id2 = edges_id(k,2);
  Ri = poses(id1).R;
  Rj = poses(id2).R;
  Rij = measurements.between(k).R;
  Errij = Rij' * Ri' * Rj;
  uth = vrrotmat2vec(Errij);
  thErr(k) = real(uth(4));
end
% figure
% plot(thErr)
cost3D = norm(thErr)^2;